FROM node:10.16.3-buster

# RUN yarn add webpack webpack-dev-server --dev

ENV NODE_ENV production

ENV THELOUNGE_HOME "/var/opt/deblounge"
VOLUME "${THELOUNGE_HOME}"

# Expose HTTP.
ENV PORT 9000
EXPOSE ${PORT}

COPY . .

RUN NODE_ENV=production yarn build

CMD ["yarn", "start"]